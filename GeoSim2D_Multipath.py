"""
Project was going to originally do multipath mitigation, but the scope changed to DoA. Some comments and code may be directed towards multipath mitigation and can be ignored.

Code which takes a random portion of a random signal and applies time delays in the frequency domain based on randomly generated geometries. The output contains three time domain signals which are the original signal with different time delays applied to each, along with angles indicating the Line-of-Sight (LOS) and Multipath (if used) angles of arrival.

Code uses signals from a directory located on the binderhub-gpu named server provided by the University of Utah Flux Research group. Signals are QAM16 generated using GNURadio. In order to use local signals, code must be modified to pull signals from the desired directory.

Antenna array configuration is determined by the parameters in the Ant_Config.yaml file. Modifying these parameters in the .yaml will also modify them when using this code.

Additional functions at the bottom of the code were written by Luke Hartung to use in the training of the DOA NN.
Any functions whose name has an appended n (_n) means that the function incorporates non-correlated gaussian noise into the output signals.

Units throughout are meters, kilograms, seconds.

Code written by Spencer Robertson - BS University of Utah
Contact info: 801-664-3351 -- masterobertson@gmail.com


List of functions:

output0,output1,output2,anglemp,anglelos = f_time(sample_size,MP,antenna_dist)

    Inputs:
        sample_size - Number of samples to run through simulator
        MP - Flag to simulate multipath (0 or 1)
        antenna_dist - Maximum x-distance between transmitter and receiver. Can be changed to increase or decrease 
        LOS angles.
    
    Outputs:
        output0 - Time domain signal seen by antenna array element 0
        output1 - Time domain signal seen by antenna array element 1
        output2 - Time domain signal seen by antenna array element 2
        anglemp - Multipath angle seen by array element 1
        anglelos - Line of sight angle seen by each array element

output0,output1,output2,fshift = f_fft(sample_size,MP)

    Inputs:
        sample_size - Number of samples to run through simulator
        MP - Flag to simulate multipath

    Outputs:
        output0 - FFT domain seen by antenna array element 0
        output1 - FFT domain seen by antenna array element 1
        output2 - FFT domain seen by antenna array element 2
        fshift - Frequency axis for plotting
    
output0,output1,output2,fshift = f_ave(mean,sample_size,MP,antenna_dist)

    Input:
        mean - Number of times to average the fft
        sample_size - Number of samples to run through simulator
        MP - Multipath Flag
        antenna_dist - Max x-distance between TX and RX

    Output:
        recAV -
        fshift - Vector for x-axis in plotting FFT
        anglelos - Line of sight angles
    
"""


import numpy as np
from matplotlib import pyplot as plt
import random
import os
import yaml
#%matplotlib notebook

#class GeoSim2D_Multipath:
    
def f_time(sample_size,MP,antenna_dist,train_flag):
    
    """
    Inputs:
        sample_size - Number of samples
        MP - Flag to simulate multipath
    """

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    
    """
    For the case with Multipath, we're going to assume the receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """

    # Load in .yaml file and set up Antenna array Configuration
    ant_elements = ant_geometry()
    
    # Setup Receiver element array (x,y-location)
    # NOTE: The middle element is listed first in the .yaml. Be sure to check your indices.
    RXy = np.zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = np.zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    # Scale from cm to m
    RXy = RXy * 1e-2
    RXx = RXx * 1e-2
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
            
    # Load in random signal from collection of signals. Filter out JT files
    if train_flag == 1:
        filenames = [_ for _ in os.listdir('/data/saved_signals/') if _.startswith("RB") or _.startswith("RF")]

        # removing files for testing from the training set
        filenames.remove("RBi16rrcNG_05")
        filenames.remove("RBi132rrcX_07")
        filenames.remove("RBi64rrcN_01")
        filenames.remove("RFi16rrcNG_035")
        filenames.remove("RFi32rrcN_07")
        filenames.remove("RFi64rrcN_05")
    else:
        filenames = [_ for _ in os.listdir('/data/saved_signals/') if _.startswith("RBi132rrcX_07") or _.startswith("RBi16rrcNG_05") or _.startswith("RBi64rrcN_01") or _.startswith("RFi16rrcNG_035") or _.startswith("RFi32rrcN_07") or _.startswith("RFi64rrcN_05")]

    filename = random.choice(filenames) # picking a random file from the list of files
    samples = np.fromfile('/data/saved_signals/'+filename, np.complex64) # Read in file.
    
    # Multipath bounce height (Decided to use integer steps for distances)
    M_bounce = random.randint(-100,0) # y-location of bounce
        
    # Generate TX geometry location
    TX = random.randint(-100, 100) # TX y-location
        
    # X-distance between TX and RX
    L = random.randint(100,antenna_dist)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    # If multipath is being simulated, indices 0-2 are for the LOS signal, indices 3-5 are for the multipath signal.
    # If multipath is not being simulated, indices 0-2 are for the LOS signal.
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    # 
    if MP == 1:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)
        D[3] = np.sqrt( (L+RXx[0])**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[4] = np.sqrt( (L+RXx[1])**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[5] = np.sqrt( (L+RXx[2])**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
    else:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)

    
    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd
    
    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements.
    """

    # Attenuation factor of multipath
    # More accurately this would take into account 1/r^2 effects.
    alpha = 0.25

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal data to perform fft
    # There's a chance of reusing the same data twice, but this is unlikely so we won't worry about it
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    # Perform FFT and shift
    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # Compute LOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if MP == 1:
        # Multipath seen at the array elements with time delay
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        # Case of no multipath
        shift3 = 0
        shift4 = 0
        shift5 = 0

    # Combine LOS and Multipath FFTs
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    # Inverse shift and ifft
    Output0 = np.fft.ifftshift(Output0)
    Output1 = np.fft.ifftshift(Output1)
    Output2 = np.fft.ifftshift(Output2)
    output0 = np.fft.ifft(Output0)
    output1 = np.fft.ifft(Output1)
    output2 = np.fft.ifft(Output2)   
        

    return output0,output1,output2,anglemp,anglelos

def f_time_n(sample_size,MP,antenna_dist,nf):
    
    """
    Inputs:
        sample_size - Number of samples
        MP - Flag to simulate multipath
    """

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """

    # Load in .yaml file and set up Antenna array Configuration
    ant_elements = ant_geometry()
    
    # Setup Receiver element array (x,y-location)
    RXy = np.zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = np.zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    # Scale to cm
    RXy = RXy * 1e-2
    RXx = RXx * 1e-2
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
            
    # Load in random signal from collection of signals. Filter out JT files
    filenames = [_ for _ in os.listdir('/data/saved_signals/') if _.startswith("RB") or _.startswith("RF")]
    filename = random.choice(filenames)
    samples = np.fromfile('/data/saved_signals/'+filename, np.complex64) # Read in file.
        
    # Multipath bounce height
    M_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate TX geometry location
    TX = random.randint(-100, 100) # TX y-location
        
    # X-distance between TX and RX
    L = random.randint(100,antenna_dist)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    if MP == 1:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)
        D[3] = np.sqrt( (L+RXx[0])**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[4] = np.sqrt( (L+RXx[1])**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[5] = np.sqrt( (L+RXx[2])**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
    else:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)

    
    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd
    
    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements.
    """

    # Attenuation factor of multipath
    alpha = 0.25

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal data to perform fft
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    # Perform FFT and shift
    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # Compute LOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if MP == 1:
        # Multipath seen at the array elements with time delay
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        # Case of no multipath
        shift3 = 0
        shift4 = 0
        shift5 = 0

    # Combine LOS and Multipath FFTs
    # NOTE: There are variables Output and output. THESE ARE NOT THE SAME.
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5
  
    N0 = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    N1 = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    N2 = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    
    Output0 = Output0+N0
    Output1 = Output1+N1
    Output2 = Output2+N2
    
#     fshift = np.linspace(-fs/2,fs/2,num=sample_size)
#     Mag0n = 10*np.log10(abs(Output0))
#     Mag1n = 10*np.log10(abs(Output1))
#     Mag2n = 10*np.log10(abs(Output2))
#     MagAn = Mag0n+Mag1n+Mag2n
    
    
#     plt.plot(fshift,MagAn)
#     plt.show()

    # Inverse shift and ifft
    Output0 = np.fft.ifftshift(Output0)
    Output1 = np.fft.ifftshift(Output1)
    Output2 = np.fft.ifftshift(Output2)
    output0 = np.fft.ifft(Output0)
    output1 = np.fft.ifft(Output1)
    output2 = np.fft.ifft(Output2)   
        

    return output0,output1,output2,anglemp,anglelos

def f_time_ave_n(mean,sample_size,MP,antenna_dist,nf,af):
    
    """
    Inputs:
        mean - number of times to average ffts
        sample_size - Number of samples to perform time delay computations on
        MP - Multipath flag
        antenna_dist - Maximum possible x-distance between TX and RX
    """

    fs = 20e6 # sampling frequency
    
    # Load in random signal from collection of signals. Filter out JT files
    filenames = [_ for _ in os.listdir('/data/saved_signals/') if _.startswith("RB") or _.startswith("RF")]
    filename = random.choice(filenames)
    samples = np.fromfile('/data/saved_signals/'+filename, np.complex64) # Read in file.

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the transceiver and receiver locations are fixed on a straight line with
    variable distance. The multipath bounce location will have a fixed point on the x-axis but a varying height on the y-axis
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds means distance between TX and RX will be fixed 3535 meters and min and max height of multipath bounce location
    will be -1767 and 1767.
    """
    # Load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    RXy = np.zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = np.zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    RXy = RXy * 1e-2
    RXx = RXx * 1e-2
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # Load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    M_bounce = np.random.randint(-1767,0) # y-location of bounce
    
    # Generate TX geometry location
    TX = np.random.randint(-100, 100) # TX y-location
   
    # X-distance between TX and RX
    L = np.random.randint(100,antenna_dist)
     
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    if MP == 1:
        D[0] = np.sqrt( (L+RXx[0])**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[1] = np.sqrt( (L+RXx[1])**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[2] = np.sqrt( (L+RXx[2])**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
        D[3] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[4] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[5] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)
    else:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Number of samples we'll take from the file
    N = sample_size
    mean = mean

    # Attenuation factor of multipath
    alpha = af

    # Create empty array for small sample file
    small_samples = np.zeros((mean,N),complex)

    # Create smaller signal to work with
    for i in range(N):
        for k in range(mean):
            small_samples[k,i] = samples[i + N*k]
    
    fsmall_samples = np.zeros((mean,N),complex)
    for k in range(mean):
        fsmall_samples[k,:] = np.fft.fft(small_samples[k,:])
        fsmall_samples[k,:] = np.fft.fftshift(fsmall_samples[k,:])

    fstemp = 2.44e9

    # Create frequency array around fs1
    fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)


    shift0 = np.zeros((mean,N),complex)
    shift1 = np.zeros((mean,N),complex)
    shift2 = np.zeros((mean,N),complex)
    shift3 = np.zeros((mean,N),complex)
    shift4 = np.zeros((mean,N),complex)
    shift5 = np.zeros((mean,N),complex)

    # Mutpath seen at the array elements   ##*****ask spencer flip multipath and LOS
    for k in range(mean):
        shift0[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
        shift1[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
        shift2[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    # LOS signal seen by the array elements with time delay
        shift3[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[5])

    ##############################
    
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5
    
    #Create a complex noise block the same size of recAV
    N0r = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    N1r = np.random.normal(0,nf,size=(Output1.shape))#.view(np.complex128) 
    N2r = np.random.normal(0,nf,size=(Output2.shape))#.view(np.complex128)
    N0i = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    N1i = np.random.normal(0,nf,size=(Output1.shape))#.view(np.complex128) 
    N2i = np.random.normal(0,nf,size=(Output2.shape))#.view(np.complex128)
    noiseR = [N0r,N1r,N2r]
    noiseI = [N0i,N1i,N2i]
    noise = np.asarray([noiseR,noiseI])
    noise = noise[0]+1j*noise[1]
   

    #add the noise before the average    

    Outputs = [Output0,Output1,Output2]
    Outputs_n = Outputs + noise
    #print(np.shape(noise))
    #print(np.shape(Outputs))
    #print(np.shape(Outputs_n))
    polarOuts = GeoAv(Outputs_n,mean)

    recAv = pol2Rec(polarOuts)
    #print(np.shape(recAv)) #Shape (2,3,512)
    aveComplex = recAv[0]+1j*recAv[1]
    
    
   
    #print(np.shape(noise))#shape(2,3,512)
    recAv = np.asarray(recAv)
    #noise = np.asarray(noise)
    recAv_n = recAv
    aveComplex_n = recAv_n[0]+1j*recAv_n[1]
  
    # Plotting for visualization
    fshift = np.linspace(-fs/2,fs/2,num=N)
    Mag0n = 10*np.log10(abs(aveComplex_n[0]))
    Mag1n = 10*np.log10(abs(aveComplex_n[1]))
    Mag2n = 10*np.log10(abs(aveComplex_n[2]))
    MagAn = Mag0n+Mag1n+Mag2n
    plt.plot(fshift,MagAn)
    plt.show()
    
    
    # Inverse shift and ifft
    Output0 = np.fft.ifftshift(aveComplex_n[0])
    Output1 = np.fft.ifftshift(aveComplex_n[1])
    Output2 = np.fft.ifftshift(aveComplex_n[2])
    output0 = np.fft.ifft(Output0)
    output1 = np.fft.ifft(Output1)
    output2 = np.fft.ifft(Output2)   
        

    return output0,output1,output2,anglemp,anglelos
    
def f_fft(sample_size,MP):
    
    """
    Inputs:
        sample_size - Number of samples
        MP - Flag to simulate multipath
    """
    
    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """

    # Load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    RXy = np.zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = np.zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    RXy = RXy*1e-2
    RXx = RXx*1e-2
    print(RXy)
    print(RXx)
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # Load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    M_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate TX geometry location
    TX = random.randint(-100, 100) # TX y-location
        
    # X-distance between TX and RX
    L = random.randint(100,5000)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    if MP == 1:
        D[0] = np.sqrt( (L+RXx[0])**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[1] = np.sqrt( (L+RXx[1])**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[2] = np.sqrt( (L+RXx[2])**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
        D[3] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[4] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[5] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)
    else:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(sample_size,complex)

    # Create smaller signal to work with
    j = random.randint(0,5000)
    for i in range(sample_size):
        small_samples[i] = samples[i+j]

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    # LOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if MP == 1:
        # Multipath seen at the array elements
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        shift3 = 0
        shift4 = 0
        shift5 = 0

    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    output0 = 10*np.log10(abs(Output0))
    output1 = 10*np.log10(abs(Output1))
    output2 = 10*np.log10(abs(Output2))
    fshift = np.linspace(-fs/2,fs/2,num=sample_size)

    """fig, axs = plt.subplots(3)
    fig.suptitle('3 Element FFT')
    axs[0].plot(fshift, Mag0,'r')
    axs[0].set(xlabel='Frequency', ylabel='1')
    axs[1].plot(fshift, Mag1,'g')
    axs[1].set(xlabel='Frequency', ylabel='2')
    axs[2].plot(fshift, Mag2,'b')
    axs[2].set(xlabel='Frequency', ylabel='3')
    plt.show()"""        

    return output0,output1,output2,fshift

    
    
    
    """
    Averaging function with local GeoAve and pol2rec function.
    Same function as f_time_ave.
    """
def f_ave(mean,sample_size,MP,antenna_dist):
                      
    """
    Inputs:
        mean - number of times to average ffts
        sample_size - Number of samples to perform time delay computations on
        MP - Multipath flag
        antenna_dist - Maximum possible x-distance between TX and RX
    """

    fs = 20e6 # sampling frequency
    
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the transceiver and receiver locations are fixed on a straight line with
    variable distance. The multipath bounce location will have a fixed point on the x-axis but a varying height on the y-axis
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds means distance between TX and RX will be fixed 3535 meters and min and max height of multipath bounce location
    will be -1767 and 1767.
    """
    # Load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    RXy = np.zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = np.zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    RXy = RXy * 1e-2
    RXx = RXx * 1e-2
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # Load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    M_bounce = np.random.randint(-1767,0) # y-location of bounce
    
    # Generate TX geometry location
    TX = np.random.randint(-100, 100) # TX y-location
   
    # X-distance between TX and RX
    L = np.random.randint(100,antenna_dist)
     
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    if MP == 1:
        D[0] = np.sqrt( (L+RXx[0])**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[1] = np.sqrt( (L+RXx[1])**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[2] = np.sqrt( (L+RXx[2])**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
        D[3] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[4] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[5] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)
    else:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Number of samples we'll take from the file
    N = sample_size
    mean = mean

    # Attenuation factor of multipath
    alpha = 0.25

    # Create empty array for small sample file
    small_samples = np.zeros((mean,N),complex)

    # Create smaller signal to work with
    for i in range(N):
        for k in range(mean):
            small_samples[k,i] = samples[i + N*k]
    
    fsmall_samples = np.zeros((mean,N),complex)
    for k in range(mean):
        fsmall_samples[k,:] = np.fft.fft(small_samples[k,:])
        fsmall_samples[k,:] = np.fft.fftshift(fsmall_samples[k,:])

    fstemp = 2.44e9

    # Create frequency array around fs1
    fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)


    shift0 = np.zeros((mean,N),complex)
    shift1 = np.zeros((mean,N),complex)
    shift2 = np.zeros((mean,N),complex)
    shift3 = np.zeros((mean,N),complex)
    shift4 = np.zeros((mean,N),complex)
    shift5 = np.zeros((mean,N),complex)

    # Mutpath seen at the array elements   ##*****ask spencer flip multipath and LOS
    for k in range(mean):
        shift0[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
        shift1[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
        shift2[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    # LOS signal seen by the array elements with time delay
        shift3[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[5])

    ##############################
    ############################## This averaging needs fixed
    ##############################
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5
    
    def GeoAv(A,mean):
        mag = np.abs(A)
        ang1 = np.angle(A)
        ang = np.unwrap(ang1,axis=1) 
        magAv = np.prod(mag,1,dtype='float64')**(1/mean)
        angAv = np.sum(ang,1,dtype='float64')/mean
        pols = [magAv, angAv]
        return pols

    def pol2Rec(A):
        real = np.zeros((np.shape(A)[1],np.shape(A)[2]))
        img = np.zeros((np.shape(A)[1],np.shape(A)[2]))
        for k in range(np.shape(A)[1]):
            for i in range(np.shape(A)[2]):
                real[k,i] = A[0][k][i]*np.cos(A[1][k][i])
                img[k,i] = A[0][k][i]*np.sin(A[1][k][i])
        back2Rec = [real, img]
        return back2Rec
         

    Outputs = [Output0,Output1,Output2]
    polarOuts = GeoAv(Outputs,mean)

    recAv = pol2Rec(polarOuts)
    #print(np.shape(recAv))
    aveComplex = recAv[0]+1j*recAv[1]

    fshift = np.linspace(-fs/2,fs/2,num=N)
    #Mag0 = 10*np.log10(abs(aveComplex[0]))
    #Mag1 = 10*np.log10(abs(aveComplex[1]))
    #Mag2 = 10*np.log10(abs(aveComplex[2]))
    #MagA = Mag0+Mag1+Mag2
    
    
    #plt.plot(fshift,MagA)
    #plt.show()


    return recAv,fshift,anglelos


    """
    Preliminary function before f_time and f_fft. Performs the same as f_fft without any outputs.
    """


def f(fs, ft, TX, L, MP, M_bounce, N):

    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the  receiver locations are fixed, the transceiver locations are random, 
    and the total x-distance between them is random.
    The multipath bounce location will have a varying height on the y-axis (We don't need the x-location for our calculations)
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds. Bounce will always come from below as this is more realistic.
    """

    # Load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    RXy = zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    RXx = RXx * 1e-2
    RXy = RXy * 1e-2
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # Load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    M_bounce = random.randint(-1767,0) # y-location of bounce
        
    # Generate TX geometry location
    TX = random.randint(-100, 100) # TX y-location
        
    # X-distance between TX and RX
    L = random.randint(100,5000)
        
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    if MP == 1:
        D[0] = np.sqrt( (L+RXx)**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[1] = np.sqrt( (L+RXx)**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[2] = np.sqrt( (L+RXx)**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
        D[3] = np.sqrt((L+RXx)**2 + (TX-RXy[0])**2)
        D[4] = np.sqrt((L+RXx)**2 + (TX-RXy[1])**2)
        D[5] = np.sqrt((L+RXx)**2 + (TX-RXy[2])**2)
    else:
        D[0] = np.sqrt((L+RXx)**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx)**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx)**2 + (TX-RXy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Attenuation factor of multipath
    alpha = 1

    # Create empty array for small sample file
    small_samples = np.zeros(N,complex)

    # Create smaller signal to work with
    for i in range(N):
        small_samples[i] = samples[i]

    print(small_samples.size)

    fsmall_samples = np.fft.fft(small_samples)
    fsmall_samples = np.fft.fftshift(fsmall_samples)

    fstemp = 2.44e9

    # Create frequency array around fs1
    fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)

    # LOS signal seen by the array elements with time delay
    shift0 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[0])
    shift1 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[1])
    shift2 = fsmall_samples*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    if MP == 1:
        # Multipath seen at the array elements
        shift3 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5 = fsmall_samples*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[5])
    else:
        shift3 = 0
        shift4 = 0
        shift5 = 0
        
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5

    Mag0 = 10*np.log10(abs(shift0+shift3))
    Mag1 = 10*np.log10(abs(shift1+shift4))
    Mag2 = 10*np.log10(abs(shift2+shift5))
    fshift = np.linspace(-(N)*fs/2,(N-1)*fs/2,num=N)

    #fig, axs = plt.subplots(3)
    #fig.suptitle('3 Element FFT')
    #axs[0].plot(fshift, Mag0,'r')
    #axs[0].set(xlabel='Frequency', ylabel='1')
    #axs[1].plot(fshift, Mag1,'g')
    #axs[1].set(xlabel='Frequency', ylabel='2')
    #axs[2].plot(fshift, Mag2,'b')
    #axs[2].set(xlabel='Frequency', ylabel='3')
    #plt.show()

    #plt.figure(figsize=(20,15))
    #plt.plot(fshift[:128],Output0[:128],'r')
    #plt.plot(fshift[:128],Output1[:128],'b')
    #plt.plot(fshift[:128],Output2[:128],'g')

    Output0 = np.fft.ifftshift(Output0)
    Output1 = np.fft.ifftshift(Output1)
    Output2 = np.fft.ifftshift(Output2)

    output0 = np.fft.ifft(Output0)
    output1 = np.fft.ifft(Output1)
    output2 = np.fft.ifft(Output2)

    return output0,output1,output2,anglemp,anglelos

"""
Duplicate of f_time_ave_n
"""

def f_ave_n(mean,sample_size,MP,antenna_dist,nf):
                      
    """
    Inputs:
        mean - number of times to average ffts
        sample_size - Number of samples to perform time delay computations on
        MP - Multipath flag
        antenna_dist - Maximum possible x-distance between TX and RX
    """

    fs = 20e6 # sampling frequency
    
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is

    # Calculate speed of light to be used in calculating time delay
    eps0 = 8.854187817e-12
    mu0 = 4*np.pi * 1e-7
    c0 = 1/np.sqrt(eps0*mu0)
    """
    For the case with Multipath, we're going to assume the transceiver and receiver locations are fixed on a straight line with
    variable distance. The multipath bounce location will have a fixed point on the x-axis but a varying height on the y-axis
    to give different angles and time delays. Assumption of max beamforming of -45 to 45 degrees and a max time delay of 
    17 microseconds means distance between TX and RX will be fixed 3535 meters and min and max height of multipath bounce location
    will be -1767 and 1767.
    """
    # Load in .yaml file for Antenna Configuration
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    ant_elements = [float(i) for i in ant_elements]
    
    # Create Receiver element array (x,y-location)
    RXy = np.zeros(3)
    RXy[1] = ant_elements[1] # Middle RX element y-location
    RXy[0] = ant_elements[4] # Location of top RX element
    RXy[2] = ant_elements[7] # Location of bottom RX element
    RXx = np.zeros(3)
    RXx[1] = ant_elements[0]
    RXx[0] = ant_elements[3]
    RXx[2] = ant_elements[6]
    
    RXy = RXy * 1e-2
    RXx = RXx * 1e-2
    
    # Sampling frequency
    fs = 20e6
    
    # Transmitting frequency
    ft = 2.44e9
    
    # Create frequency array around ft
    fs1 = np.linspace(ft-fs/2,ft+fs/2,num=sample_size)
        
    #file = np.fromfile(r'C:/Users/maste/Documents/Senior Project/Python/Signal Files',np.complex64)
    
    # Load in random signal from collection of signals
    file = random.choice(os.listdir('/data/saved_signals/'))
    samples = np.fromfile('/data/saved_signals/'+file, np.complex64) # Read in file.  We         have to tell it what format it is
        
    # Multipath bounce height
    M_bounce = np.random.randint(-1767,0) # y-location of bounce
    
    # Generate TX geometry location
    TX = np.random.randint(-100, 100) # TX y-location
   
    # X-distance between TX and RX
    L = np.random.randint(100,antenna_dist)
     
    # Calculate angles of multipath arrival
    angle = np.arctan( (2*M_bounce+TX+RXy) / (L+RXx ) )
    anglemp = angle*180/np.pi # Convert from radians to degrees
    angle = np.arctan( (TX - RXy)/(L+RXx))
    anglelos = angle*180/np.pi # Convert from radians to degrees

    # Create distance arrays dependent on multipath simulation
    if MP == 1:
        D = np.zeros(6)
    else:
        D = np.zeros(3)

    # Calculate Distances and time delays of Multipath and LOS
    if MP == 1:
        D[0] = np.sqrt( (L+RXx[0])**2 + (RXy[0]+M_bounce+(M_bounce+TX))**2 )
        D[1] = np.sqrt( (L+RXx[1])**2 + (RXy[1]+M_bounce+(M_bounce+TX))**2 )
        D[2] = np.sqrt( (L+RXx[2])**2 + (RXy[2]+M_bounce+(M_bounce+TX))**2 )
        D[3] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[4] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[5] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)
    else:
        D[0] = np.sqrt((L+RXx[0])**2 + (TX-RXy[0])**2)
        D[1] = np.sqrt((L+RXx[1])**2 + (TX-RXy[1])**2)
        D[2] = np.sqrt((L+RXx[2])**2 + (TX-RXy[2])**2)

    # Calculate time delays for each element
    td = D/c0 

    # Calculate minimum time delay
    mintd = np.min(td)

    # Subtract minimum time delay from remainder of time delays
    totaltd = td - mintd

    """
    NOTE: In order to support 17 microseconds of time delay for the multipath, the sample taken from the file must be AT LEAST
    333 elements. 512 was chosen as it is 2^9.
    """

    # Number of samples we'll take from the file
    N = sample_size
    mean = mean

    # Attenuation factor of multipath
    alpha = 0.25

    # Create empty array for small sample file
    small_samples = np.zeros((mean,N),complex)

    # Create smaller signal to work with
    for i in range(N):
        for k in range(mean):
            small_samples[k,i] = samples[i + N*k]
    
    fsmall_samples = np.zeros((mean,N),complex)
    for k in range(mean):
        fsmall_samples[k,:] = np.fft.fft(small_samples[k,:])
        fsmall_samples[k,:] = np.fft.fftshift(fsmall_samples[k,:])

    fstemp = 2.44e9

    # Create frequency array around fs1
    fs1 = np.linspace(fstemp-fs/2,fstemp+fs/2,num=N)


    shift0 = np.zeros((mean,N),complex)
    shift1 = np.zeros((mean,N),complex)
    shift2 = np.zeros((mean,N),complex)
    shift3 = np.zeros((mean,N),complex)
    shift4 = np.zeros((mean,N),complex)
    shift5 = np.zeros((mean,N),complex)

    # Mutpath seen at the array elements   ##*****ask spencer flip multipath and LOS
    for k in range(mean):
        shift0[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[0])
        shift1[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[1])
        shift2[k,:] = fsmall_samples[k,:]*alpha*np.exp(-1j*2*np.pi*fs1*totaltd[2])

    # LOS signal seen by the array elements with time delay
        shift3[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[3])
        shift4[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[4])
        shift5[k,:] = fsmall_samples[k,:]*np.exp(-1j*2*np.pi*fs1*totaltd[5])

    ##############################
    
    Output0 = shift0+shift3
    Output1 = shift1+shift4
    Output2 = shift2+shift5
    
    #Create a complex noise block the same size of recAV
    N0r = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    N1r = np.random.normal(0,nf,size=(Output1.shape))#.view(np.complex128) 
    N2r = np.random.normal(0,nf,size=(Output2.shape))#.view(np.complex128)
    N0i = np.random.normal(0,nf,size=(Output0.shape))#.view(np.complex128)
    N1i = np.random.normal(0,nf,size=(Output1.shape))#.view(np.complex128) 
    N2i = np.random.normal(0,nf,size=(Output2.shape))#.view(np.complex128)
    noiseR = [N0r,N1r,N2r]
    noiseI = [N0i,N1i,N2i]
    noise = np.asarray([noiseR,noiseI])
    noise = noise[0]+1j*noise[1]
   

    #add the noise before the average    

    Outputs = [Output0,Output1,Output2]
    Outputs_n = Outputs + noise
    #print(np.shape(noise))
    #print(np.shape(Outputs))
    #print(np.shape(Outputs_n))
    polarOuts = GeoAv(Outputs_n,mean)

    recAv = pol2Rec(polarOuts)
    #print(np.shape(recAv)) #Shape (2,3,512)
    aveComplex = recAv[0]+1j*recAv[1]
    
    
   
    #print(np.shape(noise))#shape(2,3,512)
    recAv = np.asarray(recAv)
    #noise = np.asarray(noise)
    recAv_n = recAv
    aveComplex_n = recAv_n[0]+1j*recAv_n[1]
  

    fshift = np.linspace(-fs/2,fs/2,num=N)
    Mag0n = 10*np.log10(abs(aveComplex_n[0]))
    Mag1n = 10*np.log10(abs(aveComplex_n[1]))
    Mag2n = 10*np.log10(abs(aveComplex_n[2]))
    MagAn = Mag0n+Mag1n+Mag2n
    
    
    plt.plot(fshift,MagAn)
    plt.show()


    return recAv_n,fshift,anglelos

# Geometric Averaging function
def GeoAv(A,mean):
    mag = np.abs(A)
    ang1 = np.angle(A)
    ang = np.unwrap(ang1,axis=1) 
    magAv = np.prod(mag,1,dtype='float64')**(1/mean)
    angAv = np.sum(ang,1,dtype='float64')/mean
    pols = [magAv, angAv]
    return pols

# Converts polar to rectangular
def pol2Rec(A):
    real = np.zeros((np.shape(A)[1],np.shape(A)[2]))
    img = np.zeros((np.shape(A)[1],np.shape(A)[2]))
    for k in range(np.shape(A)[1]):
        for i in range(np.shape(A)[2]):
            real[k,i] = A[0][k][i]*np.cos(A[1][k][i])
            img[k,i] = A[0][k][i]*np.sin(A[1][k][i])
            back2Rec = [real, img]
    return back2Rec

# Function to load in antenna array geometries from .yaml file
def ant_geometry():
    
    with open(r'Ant_Config.yaml') as file:
        array_geometry = yaml.load(file, Loader=yaml.FullLoader)
    
    ant_elements = []
    for i in array_geometry.values():    
        ant_elements.extend(i)
    
    # Create ant_elements which contains x,y,z coordinates for each array element
    ant_elements = [float(i) for i in ant_elements]

    return ant_elements
