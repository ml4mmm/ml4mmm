# Import Statements
import numpy as np
from matplotlib import pyplot as plt

# Function for plotting the loss curve of the NN
# Inputs: epochs and RMSE from NN
# Outputs: graph of loss curve
def plot_the_loss_curve(epochs, rmse):
    plt.figure()
    plt.xlabel("Epoch")
    plt.ylabel("Root Mean Squared Error")

    plt.plot(epochs, rmse, label="Loss")
    plt.title('Loss Curve')
    plt.legend()
    plt.ylim([rmse.min()*0.97, rmse.max()])
    plt.xscale("log")
    plt.show()
    return
   
#Function for plotting the three signals, real part of correlations, and imaginary part of correlations
def plot_cross_corr_3_elements(corr,angleofArrival,element1,element2,element3,signal):
    # Plotting the real part of the correlation
    corrREAL = np.real(np.array(corr)) # 1&2
    corrIMAG = np.imag(np.array(corr)) # 1&2

    # plotting the 3 signals on the same plot, real and imag corr plots done the same
    fig, (ax_signals, ax_real_corr, ax_imag_corr) = plt.subplots(3, 1, sharex=True)
    
    # printing AoA of the signal
    print(angleofArrival[signal]) 

    # Elements signals
    ax_signals.plot(element1[signal])
    ax_signals.plot(element2[signal])
    ax_signals.plot(element3[signal])
    ax_signals.set_title('signals from geo sim')
    ax_signals.legend(('1','2','3'))

    # Elements cross correlation real
    ax_real_corr.plot(corrREAL[0][signal])
    ax_real_corr.plot(corrREAL[1][signal])
    ax_real_corr.plot(corrREAL[2][signal])
    ax_real_corr.set_title('cross correlation REAL')
    ax_real_corr.legend(('1&2','1&3','2&3'))

    # Elements cross correlation imag
    ax_imag_corr.plot(corrIMAG[0][signal])
    ax_imag_corr.plot(corrIMAG[1][signal])
    ax_imag_corr.plot(corrIMAG[2][signal])
    ax_imag_corr.set_title('cross correlation IMAG')
    ax_imag_corr.legend(('1&2','1&3','2&3'))

    fig.tight_layout()
    fig.show()
    return

#Function for plotting the two signals, real part of correlations, and imaginary part of correlations
# Only works for cross correlation between elements 1&2
def plot_cross_corr(corr,angleofArrival,element1,element2,signal):
    # Plotting the real part of the correlation
    corrREAL = np.real(np.array(corr)) # 1&2
    corrIMAG = np.imag(np.array(corr)) # 1&2

    # plotting the everything on separate plots
    fig, (ax_signal1, ax_signal2, ax_corrREAL, ax_corrIMAG) = plt.subplots(4, 1, sharex=True)

    # printing AoA of the signal
    print(angleofArrival[signal])

    # Element 1
    ax_signal1.plot(element1[signal])
    ax_signal1.set_title('element 1 signal')

    # Element 2
    ax_signal2.plot(element2[signal])
    ax_signal2.set_title('element 2 signal')

    # cross correlation between element 1 and 2, real part
    ax_corrREAL.plot(corrREAL[0][signal])
    ax_corrREAL.set_title('real part of correlation')

    # cross correlation between element 1 and 2, imag part
    ax_corrIMAG.plot(corrIMAG[0][signal])
    ax_corrIMAG.set_title('Imaginary part of correlation')

    fig.tight_layout()
    fig.show()
    return

#Function for plotting the two signals, real part of correlations, and imaginary part of correlations
# Only works for cross correlation between elements 1&2
def plot_cross_corr_2_elements(corr,angleofArrival,element1,element2,signal):
    # Plotting the real part of the correlation
    corrREAL = np.real(np.array(corr)) # 1&2
    corrIMAG = np.imag(np.array(corr)) # 1&2

    # plotting the everything on separate plots
    fig, (ax_signal1, ax_signal2, ax_corrREAL, ax_corrIMAG) = plt.subplots(4, 1, sharex=True)

    # printing AoA of the signal
    print(angleofArrival[signal])

    # Element 1
    ax_signal1.plot(element1[signal])
    ax_signal1.set_title('element 1 signal')

    # Element 2
    ax_signal2.plot(element2[signal])
    ax_signal2.set_title('element 2 signal')

    # cross correlation between element 1 and 2, real part
    ax_corrREAL.plot(corrREAL[signal])
    ax_corrREAL.set_title('real part of correlation')

    # cross correlation between element 1 and 2, imag part
    ax_corrIMAG.plot(corrIMAG[signal])
    ax_corrIMAG.set_title('Imaginary part of correlation')

    fig.tight_layout()
    fig.show()
    return