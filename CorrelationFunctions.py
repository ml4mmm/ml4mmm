# Import Statements
import numpy as np
import scipy
import cmath
from scipy import signal

# This is the correlation function for the 2 element DOA and returns the correlation in polar form [mag,phase]
# inputs: element 1 data, element 2 data, and number of samples
# outputs: correlation in polar form
def cross_corr(element1,element2,num_sets,num_samples):
    
    # cross correlation between 2 elements
    corr = np.zeros((num_sets,num_samples),complex)  # creating a blank array to store the correlations
    for i in range(num_sets):
        corr[i] = signal.correlate(element1[i], element2[i], mode='same', method='auto')
    
    # Converting to polar from complex
    # creating an array to store the polar form of the correlations
    tempCorr = np.array(corr)
    polarCorr = np.concatenate((tempCorr, np.zeros((num_sets, num_samples))), axis=-1)

    for j in range(num_sets):
        for i in range(num_samples):
            temp = abs(tempCorr[j][i])
            temp2 = cmath.phase(tempCorr[j][i])
            polarCorr[j][i] = temp              # storing the mag of the polar in the first half of the array
            polarCorr[j][num_samples+i] = temp2 # storing the phase of the polar in the second half of the array

    polarCorr = np.real(polarCorr) # There is a + j0 term that needed to be removed for NN
    
    return polarCorr, corr

# This is the correlation function for the 3 element DOA and returns the correlation in complex form
# inputs: element 1 data, element 2 data, element 3 data, and number of samples
# outputs: correlation in complex form
def cross_corr_3_elements(element1,element2,element3,num_sets,num_samples):
    # this can work for 2-3 elements, just make a zero array the same size as the other arrays
    corr = [[], [], []]
    
    # cross correlation between 2 elements
    temp1 = np.zeros((num_sets,num_samples),complex)  # creating a blank array to store the correlations
    for i in range(num_sets):
        temp1[i] = signal.correlate(element1[i], element2[i], mode='same', method='auto')
        
    # cross correlation between 2 elements
    temp2 = np.zeros((num_sets,num_samples),complex)  # creating a blank array to store the correlations
    for i in range(num_sets):
        temp2[i] = signal.correlate(element1[i], element3[i], mode='same', method='auto')
        
    # cross correlation between 2 elements
    temp3 = np.zeros((num_sets,num_samples),complex)  # creating a blank array to store the correlations
    for i in range(num_sets):
        temp3[i] = signal.correlate(element2[i], element3[i], mode='same', method='auto')
    
    corr[0] = temp1
    corr[1] = temp2
    corr[2] = temp3
        
    return corr