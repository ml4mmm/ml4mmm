# Import Statements
import numpy as np
import cmath
from sklearn.preprocessing import MinMaxScaler

# Function for normalizing the feature and label between -1 and 1 for 2 element NN
# inputs: correlation data in polar form, angle of arrival
# outputs: normilzed features and labels
def data_prep_2_elements(polar_corr,angleofArrival):
    # Setting the label and feature
    angleofArrival = np.array(angleofArrival) # converting the AoA from list to np array 
    labels = np.round(angleofArrival) # rounding the AoA to an integer
    samples = polar_corr
    
    # scaling the NN between -1 and 1
    scaler = MinMaxScaler(feature_range=(-1,1)) # testing -1,1 was 0,1

    scaled_samples = scaler.fit_transform(samples) # this transforms the data between the Min and Max Scaler range
    scaled_labels = scaler.fit_transform(labels.reshape(-1,1)) # this transforms the data and is reshaping from (100,) to (100,1)
    
    return scaled_samples, scaled_labels

# Function for normalizing the feature and label between -1 and 1 for 3 element NN
# inputs: correlation data in polar form, angle of arrival
# outputs: normilzed features and labels

# more work will be needed to get this to work with N
# 2 elements -> 1
# 3 elements -> 3
# 4 elements -> 6
# 5 elements -> 10
# currently set up for 3 elements

def data_prep_N_elements(polar_corr,angleofArrival,num_elements,num_sets,num_samples):
    # Setting the label and feature
    angleofArrival = np.array(angleofArrival)

    labels = np.round(angleofArrival) # testing using rounding
    samples = polar_corr
    
    # scaling the NN between -1 and 1
    scaler = MinMaxScaler(feature_range=(-1,1)) # testing -1,1

    scaled_samples = samples
    for i in range(num_elements):
        scaled_samples[i] = scaler.fit_transform(samples[i])
    
    scaled_samples = scaled_samples.reshape(num_sets,num_elements,num_samples*2) # reshaping to match the label relationship
    scaled_labels = scaler.fit_transform(labels.reshape(-1,1)) #  this is reshaping from (100,) to (100,1)
    
    return scaled_samples, scaled_labels

# Function for normalizing the feature and label between -1 and 1 for N element NN
# inputs: FFT data in polar form, angle of arrival, number of elements, number of data sets, number of samples
# outputs: normilzed features and labels
def data_prep_N_elements_FFT(signals,DoA,num_elements,num_sets,num_samples):
    DoA = np.array(DoA)

    labels = np.round(DoA) # testing using rounding
    samples = signals

    # scaling the NN between -1 and 1
    scaler = MinMaxScaler(feature_range=(-1,1)) # testing -1,1 was 0,1

    # fit transform takes data in the shape [n_samples, n_features] so need to reshape data to match this
    samples = samples.reshape(num_elements,2,num_sets,num_samples) # (100, 2, 3, 512) to (3, 2, 100, 512)
    scaled_samples = samples

    for i in range(num_elements):
        for j in range(2): # this is for the real and imaginary parts
            scaled_samples[i][j] = scaler.fit_transform(samples[i][j])

    scaled_samples = scaled_samples.reshape(num_sets,2,num_elements,num_samples) # reshaping to match the label relationship
    scaled_labels = scaler.fit_transform(labels.reshape(-1,1)) #  this is reshaping from (100,) to (100,1)
    
    return scaled_samples, scaled_labels

# Converts from complex to polar form for 2 elements
# Inputs: correlation in complex form, number of data sets, number of samples
#Outputs: correlation data in polar for [Mag,...,Phase...]
def imag2polar_2_elements(corr,num_sets,num_samples):
    # Concatinating the real and imag parts of the correlation
    realCorr = np.real(np.array(corr))
    imagCorr = np.imag(np.array(corr))

    # combing the real and imaginary values [real,imag]
    complexCorr = np.concatenate((realCorr,imagCorr),axis=-1)
    
    # Converting to polar from complex
    tempCorr = np.array(corr)
    
    # addind zeros to temp corr because the data will be stored like (mag,....,polar,....)
    polarCorr = np.concatenate((tempCorr, np.zeros((num_sets,num_samples))), axis=-1)

    for j in range(num_sets):
        for i in range(num_samples):
            temp1 = abs(tempCorr[j][i])
            temp2 = cmath.phase(tempCorr[j][i])
            polarCorr[j][i]   = temp1
            polarCorr[j][num_samples+i] = temp2
            
    # There is a j0 term that needed to be removed for NN
    polarCorr = np.real(polarCorr) 
    
    return polarCorr

# Converts from complex to polar form for N elements
# Inputs: correlation in complex form, number of data sets, number of samples, and number of elements
#Outputs: correlation data in polar for [Mag,...,Phase...]

# more work will be needed to get this to work with N
# 2 elements -> 1
# 3 elements -> 3
# 4 elements -> 6
# 5 elements -> 10
# currently set up for 3 elements
def imag2polar_N_elements(corr,num_sets,num_samples,num_elements):
    # Concatinating the real and imag parts of the correlation
    realCorr = np.real(np.array(corr))
    imagCorr = np.imag(np.array(corr))

    # combing the real and imaginary values [real,imag]
    complexCorr = np.concatenate((realCorr,imagCorr),axis=2)
    
    # Converting to polar from complex
    tempCorr = np.array(corr)
    
    # addind zeros to temp corr because the data will be stored like (mag,....,polar,....)
    polarCorr = np.concatenate((tempCorr,np.zeros((num_elements,num_sets,num_samples))),axis=-1)

    for n in range(num_elements):
        for j in range(num_sets):
            for i in range(num_samples):
                temp1 = abs(tempCorr[n][j][i])
                temp2 = cmath.phase(tempCorr[n][j][i])
                polarCorr[n][j][i]   = temp1
                polarCorr[n][j][num_samples+i] = temp2
    
    # There is a j0 term that needed to be removed for NN
    polarCorr = np.real(polarCorr) 
    
    return polarCorr