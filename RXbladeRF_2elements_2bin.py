"""
Code which takes .csv files from the bladeRF and converts it to binary to give to the NN.

Spencer Robertson - 801-664-3351

"""


import numpy as np
from matplotlib import pyplot as plt
    
# Input must be in CSV format   
    
def parse_csv_file_2elements(path,step):
    
    sample_size = 512
    f = open(path,'r')
    n = 0
    file_1 = []
    file_2 = []
    
    for line in f:  # print 1st 10 lines of data-file
        list = line.split(',')
        I = int(list[0])
        Q = int(list[1])
        file_1.append(complex(I, Q))
        I = int(list[2])
        Q2 = int(list[3])
        file_2.append(complex(I, Q2))
        n += 1

    temp = (step+1)*512
    output0 = np.array(file_1[temp:(temp+512)])
    output1 = np.array(file_2[temp:(temp+512)])
        
    return output0, output1
                
                
                
